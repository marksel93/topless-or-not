<?php

namespace Ojoc\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Response;
use Ojoc\Factory\ResponseTypeFactory;
use Ojoc\Service\Config\ConfigInterface;
use Psr\Http\Message\ResponseInterface;

class OpenWeather
{
    const DEFAULT_TYPE = 'Weather';
    const ICON_URL = 'https://openweathermap.org/img/w/%s.png';

    /**
     * @var ConfigInterface
     */
    private $config;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $type;

    /**
     * OpenWeather constructor.
     *
     * @param ConfigInterface $config
     */
    public function __construct(ConfigInterface $config)
    {
        $this->config = $config;
        $this->client = $config->getHttpClient();
        $this->type = self::DEFAULT_TYPE;
    }

    /**
     * @param $type
     *
     * @return $this
     */
    public function setType(string $type)
    {
        if (!$this->isType($type)) {
            throw new \InvalidArgumentException(
                'Unknown OpenWeather type. Supported types are: ' . implode(', ', array_keys($this->getSupportedType()))
            );
        }
        $this->type = $type;

        return $this;
    }

    /**
     * @param string $type
     *
     * @return bool
     */
    private function isType(string $type): bool
    {
        if (isset($type) && !empty($type) && array_key_exists($type, $this->getSupportedType())) {
            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return array
     */
    public function getSupportedType(): array
    {
        return $this->config->supportedType();
    }

    /**
     * @param string $cityName
     *
     * @return mixed
     */
    public function getByCityName(string $cityName)
    {
        return $this->request(['query' => ['q' => $cityName]]);
    }

    /**
     * @param string $cityId
     *
     * @return mixed
     */
    public function getByCityId(string $cityId)
    {
        return $this->request(['query' => ['id' => (int)$cityId]]);
    }

    /**
     * @param float $lat
     * @param float $lon
     *
     * @return mixed
     */
    public function getByGeographicCoordinates(float $lon, float $lat)
    {
        return $this->request(['query' => ['lon' => $lon, 'lat' => $lat]]);
    }

    /**
     * @param array $parameters
     *
     * @return mixed
     */
    private function request(array $parameters)
    {
        $parameters['query']['appid'] = $this->config->apiKey();
        try {
            $data = $this->client->get($this->buildUri($this->type), $parameters);
        } catch (ClientException $e) {
            $data = new Response('');
        }

        return $this->response($this->type, $data);
    }

    /**
     * @param $requestType
     *
     * @return string
     */
    private function buildUri(string $requestType): string
    {
        $supportedType = $this->config->supportedType();

        return $this->config->version() . $supportedType[$requestType];
    }

    /**
     * Create a new response via the factory. Could be any model depending on the response type
     *
     * @param string $responseType
     * @param ResponseInterface $data
     *
     * @return mixed
     */
    private function response(string $responseType, ResponseInterface $data)
    {
        $response = new ResponseTypeFactory();

        return $response->setType($responseType)->create($data->getBody());
    }
}
