<?php

namespace Ojoc\Service\Config;

use GuzzleHttp\ClientInterface;

interface ConfigInterface
{
    /**
     * Configuration constructor.
     *
     * @param array $configuration
     */
    public function __construct(array $configuration);

    /**
     * @return string
     */
    public function baseUri(): string;

    /**
     * @param string $baseUri
     */
    public function setBaseUri(string $baseUri);

    /**
     * @return string
     */
    public function version(): string;

    /**
     * @param string $version
     */
    public function setVersion(string $version);

    /**
     * @return float
     */
    public function timeout(): float;

    /**
     * @param float $timeout
     */
    public function setTimeout(float $timeout);

    /**
     * @param ClientInterface $httpClient
     */
    public function setHttpClient(ClientInterface $httpClient);

    /**
     * @return ClientInterface
     */
    public function getHttpClient(): ClientInterface;

    /**
     * @return array
     */
    public function supportedType(): array;

    /**
     * @return string
     */
    public function apiKey(): string;
}
