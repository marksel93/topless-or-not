<?php

namespace Ojoc\Service\Config;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException;

class OpenWeatherConfig implements ConfigInterface
{
    const DEFAULT_BASE_URI = 'https://api.openweathermap.org';
    const DEFAULT_VERSION = '/data/2.5';
    const DEFAULT_TIMEOUT = 30.0;
    const DEFAULT_SUPPORTED_TYPE
        = [
            'Weather' => '/weather',
            'Forecast' => '/forecast',
        ];

    /**
     * @var string
     */
    private $baseUri;

    /**
     * @var string
     */
    private $version;

    /**
     * @var string
     */
    private $timeout;

    /**
     * HTTP Client
     */
    private $httpClient;

    /**
     * @var array
     */
    private $supportedType;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * Configuration constructor.
     *
     * @param array $configuration
     */
    public function __construct(array $configuration)
    {
        if (empty($configuration['api_key'])) {
            throw new ParameterNotFoundException('Parameter `api_key` must be set');
        }
        $this->apiKey = $configuration['api_key'];
        $this->baseUri = (isset($configuration['base_uri'])) ? $configuration['base_uri'] : self::DEFAULT_BASE_URI;
        $this->version = (isset($configuration['version'])) ? $configuration['version'] : self::DEFAULT_VERSION;
        $this->timeout = (isset($configuration['timeout'])) ? $configuration['timeout'] : self::DEFAULT_TIMEOUT;
        $this->supportedType
            = (isset($configuration['supported_request_type'])) ? $configuration['supported_request_type'] : self::DEFAULT_SUPPORTED_TYPE;
        $this->httpClient = $this->getHttpClient();
    }

    /**
     * @return string
     */
    public function baseUri(): string
    {
        return $this->baseUri;
    }

    /**
     * @param string $baseUri
     */
    public function setBaseUri(string $baseUri)
    {
        $this->baseUri = $baseUri;
    }

    /**
     * @return string
     */
    public function version(): string
    {
        return $this->version;
    }

    /**
     * @param string $version
     */
    public function setVersion(string $version)
    {
        $this->version = $version;
    }

    /**
     * @return float
     */
    public function timeout(): float
    {
        return $this->timeout;
    }

    /**
     * @param float $timeout
     */
    public function setTimeout(float $timeout)
    {
        $this->timeout = $timeout;
    }

    /**
     * @param ClientInterface $httpClient
     */
    public function setHttpClient(ClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @return Client
     */
    public function getHttpClient(): ClientInterface
    {
        if ($this->httpClient) {
            return $this->httpClient;
        }

        return $this->httpClient = $this->setDefaultHttpClient();
    }

    /**
     * @return Client
     */
    private function setDefaultHttpClient()
    {
        return new Client(
            [
                'base_uri' => self::DEFAULT_BASE_URI,
                'timeout' => self::DEFAULT_TIMEOUT,
            ]
        );
    }

    /**
     * @return array
     */
    public function supportedType(): array
    {
        return $this->supportedType;
    }

    /**
     * @return string
     */
    public function apiKey(): string
    {
        return $this->apiKey;
    }
}
