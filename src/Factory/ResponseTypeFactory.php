<?php

namespace Ojoc\Factory;

class ResponseTypeFactory
{
    const RESPONSE_MODEL_BASE = 'Ojoc\\Model\\';

    /**
     * @var string
     */
    private $type;

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setType(string $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @param string $data
     *
     * @return mixed
     */
    public function create(string $data)
    {
        if (!$this->type) {
            throw new \LogicException('Parameter $type has to be set');
        }
        if (empty($data)) {
            return null;
        }
        $responseClass = self::RESPONSE_MODEL_BASE . $this->type;
        if (!class_exists($responseClass)) {
            throw new \LogicException(
                sprintf(
                    'Class `%s` was not found. This means the configuration allows for a response type that is not configured as a model',
                    $responseClass
                )
            );
        }

        return new $responseClass(json_decode($data, true));
    }
}
