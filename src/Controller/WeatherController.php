<?php

namespace Ojoc\Controller;

use Ojoc\Service\OpenWeather;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WeatherController extends AbstractController
{
    /**
     * @var OpenWeather
     */
    private $openWeatherService;

    /**
     * WeatherController constructor.
     *
     * @param OpenWeather $openWeatherService
     */
    public function __construct(OpenWeather $openWeatherService)
    {
        $this->openWeatherService = $openWeatherService;
    }

    /**
     * @Route("/", name="weather_home")
     * @Route("/{location}", name="weather_location")
     * @param string $location
     *
     * @return Response
     */
    public function index(?string $location): Response
    {
        $location = $location ?? 'Budel-Schoot';
        $openWeatherData = $this->openWeatherService->getByCityName($location);

        return $this->render('weather/weather.html.twig', [
            'weatherData' => $openWeatherData
        ]);
    }
}
