<?php

namespace Ojoc\Model;

class Weather extends AbstractOpenWeatherModel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var array
     */
    private $coord;

    /**
     * @var array
     */
    private $weather;

    /**
     * @var string
     */
    private $base;

    /**
     * @var array
     */
    private $main;

    /**
     * @var int
     */
    private $visibility;

    /**
     * @var array
     */
    private $wind;

    /**
     * @var array
     */
    private $clouds;

    /**
     * @var int
     */
    private $dt;

    /**
     * @var array
     */
    private $sys;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Weather
     */
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Weather
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return array
     */
    public function getCoord(): array
    {
        return $this->coord;
    }

    /**
     * @param array $coord
     *
     * @return Weather
     */
    public function setCoord(array $coord)
    {
        $this->coord = $coord;

        return $this;
    }

    /**
     * @return array
     */
    public function getWeather(): array
    {
        return $this->weather;
    }

    /**
     * @param array $weather
     *
     * @return Weather
     */
    public function setWeather(array $weather)
    {
        $this->weather = $weather;

        return $this;
    }

    /**
     * @return string
     */
    public function getBase(): string
    {
        return $this->base;
    }

    /**
     * @param string $base
     *
     * @return Weather
     */
    public function setBase(string $base)
    {
        $this->base = $base;

        return $this;
    }

    /**
     * @return array
     */
    public function getMain(): array
    {
        return $this->main;
    }

    /**
     * @param array $main
     *
     * @return Weather
     */
    public function setMain(array $main)
    {
        $this->main = $main;

        return $this;
    }

    /**
     * @return int
     */
    public function getVisibility(): int
    {
        return $this->visibility;
    }

    /**
     * @param int $visibility
     *
     * @return Weather
     */
    public function setVisibility(int $visibility)
    {
        $this->visibility = $visibility;

        return $this;
    }

    /**
     * @return array
     */
    public function getWind(): array
    {
        return $this->wind;
    }

    /**
     * @param array $wind
     *
     * @return Weather
     */
    public function setWind(array $wind)
    {
        $this->wind = $wind;

        return $this;
    }

    /**
     * @return array
     */
    public function getClouds(): array
    {
        return $this->clouds;
    }

    /**
     * @param array $clouds
     *
     * @return Weather
     */
    public function setClouds(array $clouds)
    {
        $this->clouds = $clouds;

        return $this;
    }

    /**
     * @return int
     */
    public function getDt(): int
    {
        return $this->dt;
    }

    /**
     * @param int $dt
     *
     * @return Weather
     */
    public function setDt(int $dt)
    {
        $this->dt = $dt;

        return $this;
    }

    /**
     * @return array
     */
    public function getSys(): array
    {
        return $this->sys;
    }

    /**
     * @param array $sys
     *
     * @return Weather
     */
    public function setSys(array $sys)
    {
        $this->sys = $sys;

        return $this;
    }

    /**
     * @return float|string
     */
    public function getTemp()
    {
        return ($this->main) ? $this->main['temp'] : '';
    }

    /**
     * @return float|string
     */
    public function getPressure()
    {
        return ($this->main) ? $this->main['pressure'] : '';
    }

    /**
     * @return float|string
     */
    public function getHumidity()
    {
        return ($this->main) ? $this->main['humidity'] : '';
    }

    /**
     * @return float|string
     */
    public function getTempMin()
    {
        return ($this->main) ? $this->main['temp_min'] : '';
    }

    /**
     * @return float|string
     */
    public function getTempMax()
    {
        return ($this->main) ? $this->main['temp_max'] : '';
    }

    /**
     * @return float|string
     */
    public function getWindSpeed()
    {
        return ($this->wind) ? $this->wind['speed'] : '';
    }

    /**
     * @return int|string
     */
    public function getWindDeg()
    {
        return ($this->wind) ? $this->wind['deg'] : '';
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return ($this->sys) ? $this->sys['country'] : '';
    }

    /**
     * @return int|string
     */
    public function getSunrise()
    {
        return ($this->sys) ? $this->sys['sunrise'] : '';
    }

    /**
     * @return int|string
     */
    public function getSunset()
    {
        return ($this->sys) ? $this->sys['sunset'] : '';
    }
}
