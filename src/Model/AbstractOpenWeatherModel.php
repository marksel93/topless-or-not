<?php

namespace Ojoc\Model;

abstract class AbstractOpenWeatherModel
{
    /**
     * Open Weather Model constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        foreach ($data as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (property_exists($this, $key) && method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }
}
