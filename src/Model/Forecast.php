<?php

namespace Ojoc\Model;

class Forecast extends AbstractOpenWeatherModel
{
    /**
     * @var array
     */
    private $city;

    /**
     * @var string
     */
    private $message;

    /**
     * @var int
     */
    private $cnt;

    /**
     * @var array
     */
    private $list;

    /**
     * @return array
     */
    public function getCity(): array
    {
        return $this->city;
    }

    /**
     * @param array $city
     *
     * @return Forecast
     */
    public function setCity(array $city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @return Forecast
     */
    public function setMessage(string $message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return int
     */
    public function getCnt(): int
    {
        return $this->cnt;
    }

    /**
     * @param int $cnt
     *
     * @return Forecast
     */
    public function setCnt(int $cnt)
    {
        $this->cnt = $cnt;

        return $this;
    }

    /**
     * @return array
     */
    public function getList(): array
    {
        return $this->list;
    }

    /**
     * @param array $list
     *
     * @return Forecast
     */
    public function setList(array $list)
    {
        $this->list = $list;

        return $this;
    }

    /**
     * @return string
     */
    public function getCityName(): string
    {
        return ($this->city) ? $this->city['name'] : '';
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return ($this->city) ? $this->city['country'] : '';
    }
}
