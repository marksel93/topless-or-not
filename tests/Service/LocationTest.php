<?php

namespace Ojoc\Tests\Service;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Ojoc\Model\Weather;
use Ojoc\Service\OpenWeather;

class LocationTest extends WebTestCase
{
    const CITY_NAME = 'Budel-Schoot';
    const CITY_ID = 2744911;
    const CITY_COORDS
        = [
            'lon' => 5.57,
            'lat' => 51.25,
        ];

    /**
     * @var OpenWeather
     */
    private $openWeatherService;

    /**
     * Assert class
     */
    public function testWeatherType()
    {
        $weatherData = $this->getOpenWeatherService()->getByCityName(self::CITY_NAME);

        $this->assertEquals(Weather::class, get_class($weatherData));
    }

    /**
     * Assert city id by city name
     */
    public function testCityIdAndCoords()
    {
        /** @var Weather $weatherData */
        $weatherData = $this->getOpenWeatherService()->getByCityName(self::CITY_NAME);

        $this->assertEquals(self::CITY_ID, $weatherData->getId());
        $this->assertEquals(self::CITY_COORDS, $weatherData->getCoord());
    }

    /**
     * @return OpenWeather
     */
    private function getOpenWeatherService(): OpenWeather
    {
        if (!$this->openWeatherService) {
            self::bootKernel();

            $container = self::$container;
            $this->openWeatherService = $container->get('Ojoc\Service\OpenWeather');
        }

        return $this->openWeatherService;
    }
}
